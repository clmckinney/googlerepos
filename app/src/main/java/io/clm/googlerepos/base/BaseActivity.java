package io.clm.googlerepos.base;

import android.os.Bundle;
import android.app.FragmentManager;
import android.app.Fragment;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Named;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DaggerActivity;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasFragmentInjector;
import io.clm.googlerepos.R;

/**
 * Created by 123to on 3/23/2018.
 */
public abstract class BaseActivity extends DaggerActivity
{
    @Inject
    @Named(BaseActivityModule.ACTIVITY_FRAGMENT_MANAGER)
    protected FragmentManager fragmentManager;

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentInjector;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.hamburger_options_menu, menu);
        return true;
    }

    @Override
    public final AndroidInjector<Fragment> fragmentInjector()
    {
        return fragmentInjector;
    }

    // expects only res ids ints
    protected final void addFragments(@IdRes int containerViewId, Fragment fragment)
    {
        fragmentManager.beginTransaction()
                .add(containerViewId, fragment)
                .commit();
    }
}
