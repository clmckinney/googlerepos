package io.clm.googlerepos.base;

import android.os.Bundle;
import android.support.annotation.Nullable;

public interface BasePresenter
{
    void onStart(@Nullable Bundle savedInstanceState);
    void onResume();
    void onPause();
    void onSaveInstanceState(Bundle outState);
    void onEnd();
}
