package io.clm.googlerepos.base;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasFragmentInjector;

/**
 * Created by 123to on 3/25/2018.
 */

public abstract class BaseFragment extends Fragment implements HasFragmentInjector
{
    @Inject
    protected Context activityContext;

    @Inject
    DispatchingAndroidInjector<Fragment> childFragmentInjector;

    @Nullable
    private Unbinder viewUnbinder;

    // allow for minsdk target 21
    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity)
    {
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
        {
            AndroidInjection.inject(this);
        }
        super.onAttach(activity);
    }

    @Override
    public void onAttach(Context context)
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            AndroidInjection.inject(this);
        }
        super.onAttach(context);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onViewStateRestored(Bundle savedInstanceState)
    {
        super.onViewStateRestored(savedInstanceState);
        viewUnbinder = ButterKnife.bind(this, getView());
    }

    @Override
    public void onDestroyView()
    {
        // unbind if frag has ui
        if(viewUnbinder != null)
        {
            viewUnbinder.unbind();
        }
        super.onDestroyView();
    }

    @Override
    public final AndroidInjector<Fragment> fragmentInjector()
    {
        return childFragmentInjector;
    }

}
