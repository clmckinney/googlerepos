package io.clm.googlerepos.base;

import android.os.Bundle;
import android.support.annotation.Nullable;

import io.clm.googlerepos.view.MVPView;

public class BasePresenterImpl<T extends MVPView> implements BasePresenter
{
    protected final T view;

    protected BasePresenterImpl(T view)
    {
        this.view = view;
    }

    @Override
    public void onStart(@Nullable Bundle savedInstanceState)
    {}

    @Override
    public void onResume()
    {}

    @Override
    public void onPause()
    {}

    @Override
    public void onSaveInstanceState(Bundle ouState)
    {}

    @Override
    public void onEnd()
    {}
}
