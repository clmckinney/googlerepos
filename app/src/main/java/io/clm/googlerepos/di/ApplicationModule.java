package io.clm.googlerepos.di;

import android.app.Activity;

import dagger.Binds;
import dagger.Module;
import dagger.android.ActivityKey;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;
import dagger.multibindings.IntoMap;
import io.clm.googlerepos.main.MainActivity;
import io.clm.googlerepos.main.MainActivitySubcomponent;

@Module(includes = {
        AndroidInjectionModule.class,
},
        subcomponents = {
                MainActivitySubcomponent.class
        })
abstract class ApplicationModule
{
    @Binds
    @IntoMap
    @ActivityKey(MainActivity.class)
    abstract AndroidInjector.Factory<? extends Activity> mainActivityInjectorFactory(MainActivitySubcomponent.Builder builder);
}
