package io.clm.googlerepos.di;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.Provides;
import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import io.clm.googlerepos.GoogleReposApplication;
import io.clm.googlerepos.di.ApplicationModule;
import io.clm.googlerepos.network.NetworkModule;

@Singleton
@Component(modules = {
        ApplicationModule.class,
        NetworkModule.class
})
public interface ApplicationComponent extends AndroidInjector<GoogleReposApplication>
{
//    Context provideContext();

//    @Provides
    void inject(GoogleReposApplication application);
//
//    @Override
//    void inject(DaggerApplication instance);
//
//    @dagger.Component.Builder
//    abstract class Builder extends AndroidInjector.Builder<GoogleReposApplication>{}
}
