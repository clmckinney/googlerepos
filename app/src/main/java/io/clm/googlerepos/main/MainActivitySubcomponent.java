package io.clm.googlerepos.main;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import io.clm.googlerepos.di.PerActivity;

@PerActivity
@Subcomponent(modules = {
        MainActivityModule.class
})
public interface MainActivitySubcomponent extends AndroidInjector<MainActivity>
{
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<MainActivity>
    {}
}
