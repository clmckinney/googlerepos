package io.clm.googlerepos.main;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.widget.Toolbar;

import butterknife.BindView;
import io.clm.googlerepos.R;
import io.clm.googlerepos.home.AllReposFragment;
import io.clm.googlerepos.base.BaseActivity;

public class MainActivity extends BaseActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        FragmentManager fragmentManager = getFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.screen_container);
        if(fragment == null)
        {
            fragment = new AllReposFragment();
            fragmentManager.beginTransaction().add(R.id.screen_container, fragment)
                    .commit();
        }
    }

}
