package io.clm.googlerepos.main;

import android.app.Activity;
import android.app.Fragment;

import dagger.Binds;
import dagger.Module;
import dagger.android.AndroidInjector;
import dagger.android.FragmentKey;
import dagger.multibindings.IntoMap;
import io.clm.googlerepos.home.AllReposFragment;
import io.clm.googlerepos.home.AllReposFragmentSubcomponent;
import io.clm.googlerepos.base.BaseActivityModule;
import io.clm.googlerepos.di.PerActivity;
import io.clm.googlerepos.repo_details.RepoDetailsFragment;
import io.clm.googlerepos.repo_details.RepoDetailsFragmentSubcomponent;

@Module(includes = {
        BaseActivityModule.class
},
subcomponents = {
        AllReposFragmentSubcomponent.class,
        RepoDetailsFragmentSubcomponent.class
})
abstract class MainActivityModule
{
    @Binds
    @IntoMap
    @FragmentKey(AllReposFragment.class)
    abstract AndroidInjector.Factory<? extends Fragment> homeFragmentInjectorFactory(AllReposFragmentSubcomponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(RepoDetailsFragment.class)
    abstract AndroidInjector.Factory<? extends Fragment> repoDetailsFragmentInjectorFactory(RepoDetailsFragmentSubcomponent.Builder builder);

    @Binds
    @PerActivity
    abstract Activity activity(MainActivity mainActivity);

}
