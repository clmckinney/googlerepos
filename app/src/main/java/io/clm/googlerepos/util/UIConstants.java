package io.clm.googlerepos.util;

public final class UIConstants
{
    public final static String LAST_UPDATE_LABEL = "Last Update:";
    public final static String OWNER_LABEL = "Owner:";
}
