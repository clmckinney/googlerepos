package io.clm.googlerepos.repo_details;

import android.app.Fragment;

import javax.inject.Named;

import dagger.Binds;
import dagger.Module;
import io.clm.googlerepos.base.BaseFragmentModule;
import io.clm.googlerepos.di.PerFragment;

@Module(includes = {
        BaseFragmentModule.class
})
public abstract class RepoDetailsFragmentModule
{
    @Binds
    @Named(BaseFragmentModule.FRAGMENT)
    @PerFragment
    abstract Fragment fragment(RepoDetailsFragment repoDetailsFragment);
}
