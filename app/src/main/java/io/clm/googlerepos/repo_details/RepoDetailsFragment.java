package io.clm.googlerepos.repo_details;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.clm.googlerepos.R;
import io.clm.googlerepos.base.BaseFragment;
import io.clm.googlerepos.home.AllReposFragment;
import io.clm.googlerepos.models.all_repos_model.AllReposResponse;

public class RepoDetailsFragment extends BaseFragment
{
    AllReposResponse repo;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.repo_name)
    TextView repoNameTv;
    @BindView(R.id.star_count)
    TextView repoStarCountTv;
    @BindView(R.id.fork_count)
    TextView repoForkCountTv;
    @BindView(R.id.repo_description)
    TextView repoDescriptionTv;
    @BindView(R.id.repo_created_date)
    TextView repoCreatedDateTv;
    @BindView(R.id.repo_last_updated)
    TextView repoLastUpdatedTv;
    @BindView(R.id.repo_link)
    TextView repoLinkTv;
    @BindView(R.id.repo_license_key)
    TextView repoLicenseKeyTv;
    @BindView(R.id.repo_license_name)
    TextView repoLicenseNameTv;

    String repoName;
    String repoStarCount;
    String repoForkCount;
    String repoDescription;
    String repoCreatedDate;
    String repoLastUpdated;
    String repoLink;
    String repoLicenseKey;
    String repoLicenseName;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.screen_repo_details, container, false);

        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        getActivity().setActionBar(toolbar);
        setHasOptionsMenu(true);

        repoName = repo.getName();
        repoStarCount = Long.toString(repo.getStargazers_count());
        repoForkCount = Long.toString(repo.getForks_count());
        repoDescription = repo.getDescription();
        repoCreatedDate = repo.getCreated_at();
        repoLastUpdated = repo.getUpdated_at();
        repoLink = repo.getHtmlUrl();
        repoLicenseKey = repo.getLicense().getKey();
        repoLicenseName = repo.getLicense().getLicenseName();

        repoNameTv.setText(repoName);
        repoStarCountTv.setText(repoStarCount);
        repoForkCountTv.setText(repoForkCount);
        repoDescriptionTv.setText(repoDescription);
        repoCreatedDateTv.setText(repoCreatedDate);
        repoLastUpdatedTv.setText(repoLastUpdated);

        repoLinkTv.setClickable(true);
        repoLinkTv.setMovementMethod(LinkMovementMethod.getInstance());
        String link = String.format("<a href=\"%s\">%s</a>",repoLink,repoLink);
        repoLinkTv.setText(Html.fromHtml(link));
        repoLinkTv.setMovementMethod(LinkMovementMethod.getInstance());

        repoLicenseKeyTv.setText(repoLicenseKey);
        repoLicenseNameTv.setText(repoLicenseName);
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {
        int id = menuItem.getItemId();
        switch(id)
        {
            case R.id.home :
                System.out.println("home clicked");
                AllReposFragment allReposFragment = new AllReposFragment();
                getFragmentManager().beginTransaction()
                        .replace(R.id.screen_container, allReposFragment)
                        .addToBackStack(null)
                        .commit();
            case R.id.logout:

                break;
        }

        return super.onOptionsItemSelected(menuItem);
    }

    public void setRepo(AllReposResponse repo)
    {
        this.repo = repo;
    }
}
