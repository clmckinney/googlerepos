package io.clm.googlerepos.repo_details;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import io.clm.googlerepos.di.PerFragment;

@PerFragment
@Subcomponent(modules = {
        RepoDetailsFragmentModule.class
})
public interface RepoDetailsFragmentSubcomponent extends AndroidInjector<RepoDetailsFragment>
{
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<RepoDetailsFragment>
    {}
}
