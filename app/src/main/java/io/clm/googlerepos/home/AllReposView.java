package io.clm.googlerepos.home;

import java.util.List;

import io.clm.googlerepos.models.all_repos_model.AllReposResponse;
import io.clm.googlerepos.view.MVPView;

public interface AllReposView extends MVPView
{
    void loadRV(List<AllReposResponse> repos);
}
