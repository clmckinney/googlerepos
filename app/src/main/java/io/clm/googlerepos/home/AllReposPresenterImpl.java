package io.clm.googlerepos.home;

import javax.inject.Inject;

import io.clm.googlerepos.base.BasePresenterImpl;
import io.clm.googlerepos.di.PerFragment;

@PerFragment
public class AllReposPresenterImpl extends BasePresenterImpl<AllReposView> implements AllReposPresenter
{
    @Inject
    AllReposPresenterImpl(AllReposView view)
    {
        super(view);
    }

    @Override
    public void callService()
    {

    }
}
