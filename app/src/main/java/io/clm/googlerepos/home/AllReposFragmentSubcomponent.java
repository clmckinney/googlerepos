package io.clm.googlerepos.home;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import io.clm.googlerepos.di.PerFragment;

@PerFragment
@Subcomponent(modules = {
        AllReposFragmentModule.class
})
public interface AllReposFragmentSubcomponent extends AndroidInjector<AllReposFragment>
{
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<AllReposFragment>
    {

    }

}
