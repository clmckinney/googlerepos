package io.clm.googlerepos.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toolbar;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.clm.googlerepos.R;
import io.clm.googlerepos.adapters.AllReposAdapter;
import io.clm.googlerepos.base.BaseFragment;
import io.clm.googlerepos.data.repos.ReposClent;
import io.clm.googlerepos.data.repos.ReposService;
import io.clm.googlerepos.models.all_repos_model.AllReposResponse;
import io.clm.googlerepos.repo_details.RepoDetailsFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllReposFragment extends BaseFragment implements AllReposAdapter.ItemClickListener, AllReposView
{
    ReposService service;

    List<AllReposResponse> repos = null;
    Call<List<AllReposResponse>> callAsync = null;
    AllReposAdapter adapter;
    RecyclerView recyclerView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.loading_indicator)
    ProgressBar progessBar;

    @BindView(R.id.error_tv)
    TextView errotTv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.screen_google_repos, container, false);

        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        getActivity().setActionBar(toolbar);
        setHasOptionsMenu(true);

        recyclerView = view.findViewById(R.id.rvRepos);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        if (repos == null)
        {
            callService();
        } else
        {
            loadRV(repos);
        }
    }

    public void callService()
    {
        progessBar.setVisibility(View.VISIBLE);
        errotTv.setVisibility(View.GONE);

        service = ReposClent.getClient().create(ReposService.class);
        callAsync = service.getAllRepos();
        callAsync.enqueue(new Callback<List<AllReposResponse>>()
        {

            @Override
            public void onResponse(Call<List<AllReposResponse>> call, Response<List<AllReposResponse>> response)
            {
                repos = response.body();
                loadRV(repos);
                progessBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<List<AllReposResponse>> call, Throwable t)
            {
                errotTv.setVisibility(View.VISIBLE);
                progessBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void loadRV(List<AllReposResponse> repos)
    {
        this.repos = repos;
        adapter = new AllReposAdapter(getActivity(), repos);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(View view, int position)
    {
        RepoDetailsFragment repoDetailsFragment = new RepoDetailsFragment();
        getFragmentManager().beginTransaction()
                .replace(R.id.screen_container, repoDetailsFragment)
                .addToBackStack(null)
                .commit();
        repoDetailsFragment.setRepo(repos.get(position));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {
        int id = menuItem.getItemId();
        switch (id)
        {
            case R.id.home:
                AllReposFragment allReposFragment = new AllReposFragment();
                getFragmentManager().beginTransaction()
                        .replace(R.id.screen_container, allReposFragment)
                        .addToBackStack(null)
                        .commit();
            case R.id.logout:

                break;
        }

        return super.onOptionsItemSelected(menuItem);
    }
}
