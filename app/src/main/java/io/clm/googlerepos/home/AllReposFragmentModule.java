package io.clm.googlerepos.home;

import dagger.Binds;
import dagger.Module;
import io.clm.googlerepos.base.BaseFragmentModule;
import io.clm.googlerepos.di.PerFragment;

@Module(includes = {
        BaseFragmentModule.class
})
abstract class AllReposFragmentModule
{
//    @Binds
//    @Named(BaseFragmentModule.FRAGMENT)
//    @PerFragment
//    abstract Fragment fragment(AllReposFragment allReposFragment);
//
    @Binds
    @PerFragment
    abstract AllReposView allReposView(AllReposFragment allReposFragment);
}
