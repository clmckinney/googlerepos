package io.clm.googlerepos.data.repos;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import io.clm.googlerepos.models.all_repos_model.AllReposResponse;
import retrofit2.Response;

public class ReposRequester
{
    private final ReposService reposService;

    @Inject
    ReposRequester(ReposService reposService)
    {
        this.reposService = reposService;
    }

    public Response<List<AllReposResponse>> getAllRepos() throws IOException
    {
        return reposService.getAllRepos().execute();
    }
}
