package io.clm.googlerepos.data.repos;


import java.util.List;

import io.clm.googlerepos.models.all_repos_model.AllReposResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface ReposService
{
    @Headers("User-Agent: googlerepos-app")
    @GET("users/google/repos?")
    Call<List<AllReposResponse>> getAllRepos();

    @Headers("User-Agent: googlerepos-app")
    @GET("users/repos/{name}?")
    Call<List<AllReposResponse>> getRepo(@Path("name") String repoName);
}
