package io.clm.googlerepos.models.all_repos_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class License
{
    @SerializedName("key")
    @Expose
    String key;
    @SerializedName("name")
    @Expose
    String licenseName;
    @SerializedName("spdx_id")
    @Expose
    String spdxId;
    @SerializedName("url")
    @Expose
    String url;

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public String getLicenseName()
    {
        return licenseName;
    }

    public void setLicenseName(String licenseName)
    {
        this.licenseName = licenseName;
    }

    public String getSpdxId()
    {
        return spdxId;
    }

    public void setSpdxId(String spdxId)
    {
        this.spdxId = spdxId;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }
}
