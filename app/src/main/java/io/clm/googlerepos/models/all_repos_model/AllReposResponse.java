package io.clm.googlerepos.models.all_repos_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AllReposResponse
{
    @SerializedName("id")
    @Expose
    Long id;
    @SerializedName("name")
    @Expose
    String name;
    @SerializedName("full_name")
    @Expose
    String full_name;
    @SerializedName("owner")
    @Expose
    Owner owner;
    @SerializedName("html_url")
    @Expose
    String htmlUrl;
    @SerializedName("description")
    @Expose
    String description;
    @SerializedName("created_at")
    @Expose
    String created_at;
    @SerializedName("updated_at")
    @Expose
    String updated_at;
    @SerializedName("stargazers_count")
    @Expose
    long stargazers_count;
    @SerializedName("watchers_count")
    @Expose
    long watchers_count;
    @SerializedName("forks_count")
    @Expose
    long forks_count;
    @SerializedName("license")
    @Expose
    License license;
    @SerializedName("watchers")
    @Expose
    long watchers;

    /*
     * @return
     */
    public Long getId()
    {
        return id;
    }

    /*
     * @param id
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /*
     * @return
     */
    public String getName()
    {
        return name;
    }

    /*
     * @param name
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /*
     * @return
     */
    public String getFull_name()
    {
        return full_name;
    }

    /*
     * @param full_name
     */
    public void setFull_name(String full_name)
    {
        this.full_name = full_name;
    }

    /*
     * @return
     */
    public Owner getOwner()
    {
        return owner;
    }

    /*
     * @param owner
     */
    public void setOwner_name(Owner owner)
    {
        this.owner = owner;
    }

    /*
     * @return
     */
    public String getDescription()
    {
        return description;
    }

    /*
     * @param description
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /*
     * @return
     */
    public String getCreated_at()
    {
        return created_at;
    }

    /*
     * @param created_at
     */
    public void setCreated_at(String created_at)
    {
        this.created_at = created_at;
    }

    /*
     * @return
     */
    public String getUpdated_at()
    {
        return updated_at;
    }

    /*
     * @param updated_at
     */
    public void setUpdated_at(String updated_at)
    {
        this.updated_at = updated_at;
    }

    /*
     * @return
     */
    public long getStargazers_count()
    {
        return stargazers_count;
    }

    /*
     * @param stargazers_count
     */
    public void setStargazers_count(long stargazers_count)
    {
        this.stargazers_count = stargazers_count;
    }

    /*
     * @return
     */
    public long getWatchers_count()
    {
        return watchers_count;
    }

    /*
     * @param watchers_count
     */
    public void setWatchers_count(long watchers_count)
    {
        this.watchers_count = watchers_count;
    }

    /*
     * @return
     */
    public long getForks_count()
    {
        return forks_count;
    }

    /*
     * @param forks_count
     */
    public void setForks_count(long forks_count)
    {
        this.forks_count = forks_count;
    }

    /*
     * @return
     */
    public long getWatchers()
    {
        return watchers;
    }

    /*
     * @param watchers
     */
    public void setWatchers(long watchers)
    {
        this.watchers = watchers;
    }

    public void setOwner(Owner owner)
    {
        this.owner = owner;
    }

    public License getLicense()
    {
        return license;
    }

    public void setLicense(License license)
    {
        this.license = license;
    }

    public String getHtmlUrl()
    {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl)
    {
        this.htmlUrl = htmlUrl;
    }
}
