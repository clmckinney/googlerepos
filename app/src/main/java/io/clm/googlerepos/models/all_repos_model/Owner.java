package io.clm.googlerepos.models.all_repos_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Owner
{
    @SerializedName("login")
    @Expose
    String login;

    public String getLogin()
    {
        return login;
    }

    public void setLogin(String login)
    {
        this.login = login;
    }
}
