package io.clm.googlerepos.network;

import java.io.File;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.clm.googlerepos.data.repos.ReposService;
import io.clm.googlerepos.util.RestConstants;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule
{
    File cacheFile;

    public NetworkModule(File cacheFile)
    {
        this.cacheFile = cacheFile;
    }

    @Provides
    @Singleton
    Retrofit provideReposCall()
    {
        Cache cache = new Cache(cacheFile, 10 * 1024 * 1024);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder().cache(cache);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .client(httpClient.build())
                .build();

        return retrofit;
    }

    @Provides
    @Singleton
    @SuppressWarnings("unused")
    public ReposService provideReposNetworkService(Retrofit retrofit)
    {
        return retrofit.create(ReposService.class);
    }

//    @Provides
//    @Singleton
//    @SuppressWarnings("unused")
//    public ReposService provideReposService(ReposService reposService)
//    {
//        return new AllReposService2(reposService);
//    }
}
