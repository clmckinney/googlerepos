package io.clm.googlerepos;

import android.app.Activity;
import android.app.Application;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import io.clm.googlerepos.di.DaggerApplicationComponent;

public class GoogleReposApplication extends Application implements HasActivityInjector//extends DaggerApplication
{
//    @Inject
//    ActivityInj
//    ApplicationComponent component;

    @Inject
    DispatchingAndroidInjector<Activity> activityInjector;

    @Override
    public void onCreate()
    {
        super.onCreate();

//        component = DaggerApplicationComponent.builder()
//                .applicationModule(new ApplicationModule(this))
//                .build();
//        component.inject(this);

        DaggerApplicationComponent.create().inject(this);

//        if(BuildConfig.DEBUG) {
//            Timber.plant(new Timber.DebugTree());
//        }
    }

//    public ActivityInjector getActivityInjector() {
//        return activityInjector;
//    }

    @Override
    public AndroidInjector<Activity> activityInjector()
    {
        return activityInjector;
    }
}
