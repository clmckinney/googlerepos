package io.clm.googlerepos.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.clm.googlerepos.R;
import io.clm.googlerepos.models.all_repos_model.AllReposResponse;
import io.clm.googlerepos.util.UIConstants;

public class AllReposAdapter extends RecyclerView.Adapter<AllReposAdapter.ViewHolder>
{
    private List<AllReposResponse> allReposResponseList;
    private LayoutInflater inflater;
    private ItemClickListener clickListener;

    public AllReposAdapter(Context context, List<AllReposResponse> allReposResponseList)
    {
        this.inflater = LayoutInflater.from(context);
        this.allReposResponseList = allReposResponseList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = inflater.inflate(R.layout.view_repo_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        String repoName = allReposResponseList.get(position).getName();
        holder.nameTextView.setText(repoName);
        String ownerLabel = UIConstants.OWNER_LABEL;
        holder.ownerLabelTextView.setText(ownerLabel);
        String ownerName = allReposResponseList.get(position).getOwner().getLogin();
        holder.ownerNameTextView.setText(ownerName);
        String lastUpdatedLabel = UIConstants.LAST_UPDATE_LABEL;
        holder.lastUpdatedLabelTextView.setText(lastUpdatedLabel);
        String lastUpdated = allReposResponseList.get(position).getUpdated_at();
        holder.lastUpdatedTextView.setText(lastUpdated);
    }

    @Override
    public int getItemCount()
    {
        return allReposResponseList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        @BindView(R.id.tv_repo_name)
        TextView nameTextView;
        @BindView(R.id.tv_owner_name_label)
        TextView ownerLabelTextView;
        @BindView(R.id.tv_repo_owner_name)
        TextView ownerNameTextView;
        @BindView(R.id.tv_last_updated_label)
        TextView lastUpdatedLabelTextView;
        @BindView(R.id.tv_last_updated)
        TextView lastUpdatedTextView;

        ViewHolder(View itemView)
        {
            super(itemView);
            itemView.setOnClickListener(this);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onClick(View view)
        {
            if (clickListener != null) clickListener.onItemClick(view, getAdapterPosition());
        }
    }

    public long getItem(int id)
    {
        return allReposResponseList.get(id).getId();
    }

    public void setClickListener(ItemClickListener itemClickListener)
    {
        this.clickListener = itemClickListener;
    }

    public interface ItemClickListener
    {
        void onItemClick(View view, int position);
    }
}
