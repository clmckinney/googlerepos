package io.clm.googlerepos.main.data.all_repos;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import io.clm.googlerepos.data.repos.ReposService;
import io.clm.googlerepos.util.RestConstants;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ReposService2Test
{
    Retrofit retrofit = null;
    ReposService reposService = null;
    Call<List<ReposService>> call = null;
    Response<List<ReposService>> response = null;
    List<ReposService> repos = null;

    @Before
    public void prepareTest()
    {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        retrofit = new Retrofit.Builder()
                .baseUrl(RestConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        reposService = retrofit.create(ReposService.class);
    }

    @Test
    public void testCallAllRepos() throws IOException
    {
//        response = reposService.getRepos().execute();
//
//        Assert.assertEquals(true, response.isSuccessful());
//
//        repos = response.body();
//        System.out.println("list created");
//        Assert.assertEquals(30, repos.size());
//
//        long expectedId = 91820777;
//        long actualId = repos.get(0).getRepos().;
//        Assert.assertEquals("Error in matching the first ID found",expectedId, actualId);
    }
}
