# GoogleRepos

This is a small application using the GitHub API to display the available Google repositories and the details of
each repository. The libraries this project uses are Dagger2, Butterknife, Gson, and Retrofit. This project uses 
and MVP architecture. 